package com.example.tugas5papb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bt1;
    Button bt2;
    EditText et1;
    RecyclerView rv;
    RVAdapter rvAdapter;
    List<Item> itemList;
    private AppDatabase appDb;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Tugas 5 PAPB | 215150401111037");
        appDb = AppDatabase.getInstance(getApplicationContext());
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        rv = findViewById(R.id.rv);
        et1 = findViewById(R.id.et1);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rvAdapter = new RVAdapter(itemList, this);
        rv.setAdapter(rvAdapter);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                Item item = new Item();
                item.setId(i);
                item.setJudul(et1.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        itemList = appDb.itemDao().getAll();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                rvAdapter.setItemList(itemList);
                            }
                        });
                    }
                });
            }
        });
    }
}