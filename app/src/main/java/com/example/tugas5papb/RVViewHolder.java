package com.example.tugas5papb;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RVViewHolder extends RecyclerView.ViewHolder{

    TextView tvNama;
    public RVViewHolder(@NonNull View itemView) {
        super(itemView);
        tvNama = itemView.findViewById(R.id.tvNama);
    }
}