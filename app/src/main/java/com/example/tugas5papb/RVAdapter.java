package com.example.tugas5papb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVViewHolder>{

    Context _context;
    List<Item> items;
    public RVAdapter(List<Item> itemList, Context context) {
        this.items = itemList;
        this._context = context;
    }

    @NonNull
    @Override
    public RVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(_context).inflate(R.layout.card, parent, false);
        return new RVViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVViewHolder holder, int position) {
        Item item = items.get(position);
        holder.tvNama.setText(item.getJudul());
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    public void setItemList(List<Item> itemList) {
        this.items = itemList;
        notifyDataSetChanged();
    }
}

